Source: di
Section: utils
Priority: optional
Maintainer: Michael Ablassmeier <abi@debian.org>
Build-Depends: debhelper (>= 13),
               debhelper-compat (=13),
               cmake,
               gettext,
               libtirpc-dev,
               libtommath-dev,
               pkgconf
Standards-Version: 4.7.0
Homepage: https://diskinfo-di.sourceforge.io/
Vcs-Git: https://salsa.debian.org/debian/di.git
Vcs-Browser: https://salsa.debian.org/debian/di
Rules-Requires-Root: no

Package: di
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: advanced df like disk information utility
 `di' is a disk information utility, displaying everything
 (and more) that your `df' command does. It features the
 ability to display your disk usage in whatever format you
 desire/prefer/are used to. It is designed to be portable
 across many platforms.

Package: libdi-dev
Architecture: any
Depends: libdi5 (= ${binary:Version}), ${misc:Depends}
Section: libdevel
Description: Header files for libdi
 Development files for libdi, a shared library that can be used to get disk
 space or percentages.

Package: libdi5
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Section: libs
Description: Common libraries for di
 The libdi package provides the shared libraries for the advanced
 disk information utility
